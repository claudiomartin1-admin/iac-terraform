terraform {
    backend "s3" {
        bucket       = "cmbawsterraformbucket"
        key          = "terraform.tfstate"
        region       = "us-west-2"
    }
}

provider "aws" {
  region     = "us-west-2"
  # access_key = var.AWS_ACCESS_KEY
  # secret_key = var.AWS_SECRET_KEY
}

resource "aws_instance" "us-west-2" {
  ami           = "ami-005e54dee72cc1d00" # us-west-2
  instance_type = "t2.micro"
  tags            = {
      Name = "InfraAsCodeV2"
  }
}